#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include <std_msgs/String.h>
#include <stdio.h>
#include "nodelet_path_smoothing/OutputData.h"
#include "nodelet_path_smoothing/OutputPath.h"
#include "nodelet_path_smoothing/OutputTraj.h"

#include "nodelet_talker/InputData.h"

#include "math.h"
#include <vector>
#include <list>
#include <sstream>

#include "Traj/PathSmoothing.h"
#include "Traj/TrajPathProfile.h"
#include "Traj/ClothoidPathProfile.h"


namespace nodelet_path_smoothing
{
class path_smoothing: public nodelet::Nodelet, public C2DTrajectoryDataTypes
{
public:
    ros::Publisher pub_result, pub_path_result, pub_traj_result;
    ros::Subscriber sub_chat;   
    //typedef double T;
    double Delta = 0.01;
    int completed = 0;
    //int pub = 0;
    virtual void onInit() {  
        ros::NodeHandle& private_nh = getPrivateNodeHandle();
        pub_result = private_nh.advertise<OutputData>("result",10);
        pub_path_result = private_nh.advertise<OutputPath>("SmoothedPathPub",10);
        pub_traj_result = private_nh.advertise<OutputTraj>("SmoothedTrajPub",10);
        sub_chat = private_nh.subscribe<nodelet_talker::InputData>("msg_in", 1, &path_smoothing::callback, this);
    }    


    void callback(const nodelet_talker::InputData::ConstPtr& input)
    {
        C2DTrajectoryBase::S2DTrajPoint* goalState = (C2DTrajectoryBase::S2DTrajPoint*)input->GoalStatePtr;
        C2DTrajectoryBase::S2DTrajPoint* currentState = (C2DTrajectoryBase::S2DTrajPoint*)input->CurrentStatePtr;
        std::vector<Geom::C2DPoint >* PWLinearPath= (std::vector<Geom::C2DPoint >*)input->PWLinearPathPtr;

        if(!C2DClothoidApp::CreateLookupTable())
            return;
					const clock_t begin_time = clock();

        CClothoidPathSmoothing PathSmoothing;
        PathSmoothing.SmoothPath((*currentState), (*goalState), (*PWLinearPath));
        CTrajCompositePathProfile& SmoothedPath = PathSmoothing.SmoothedPath();    


	float timebefore = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
			ROS_INFO("vrijeme za replaniranje: %f", timebefore);
			FILE *timefile;
			timefile = fopen("timeReplanning","a");
			fprintf(timefile,"%f \n", timebefore);
			fclose(timefile);
        OutputData data;
        OutputPath path_data;
        OutputTraj traj_data;
        geometry_msgs::Point p; 
        geometry_msgs::Twist vel;

        traj_data.t.clear();
        traj_data.points.clear();
        traj_data.th.clear();
        traj_data.velocities.clear();
        traj_data.curv.clear();

        path_data.points.clear();

        data.SmoothedPathPtr=(long)(&SmoothedPath);
        pub_result.publish(data);   //objavljivanje poruke na temu

        C2DTrajectoryBase::S2DTrajPoint TrajPt;
        TrajPt.SetAllZero();
        // spremanje po putu
        C2DTrajectoryDataTypes::T Dist1 = SmoothedPath.GetBeginDist();
        C2DTrajectoryDataTypes::T Dist2 = SmoothedPath.GetEndDist();
        
        ROS_INFO("pocetak trajektorije na %f, kraj trajektorije na %f", Dist1, Dist2);
        for(C2DTrajectoryDataTypes::T Dist = Dist1; Dist <= Dist2; Dist += Delta)
        {
            TrajPt.Dist = Dist;
            SmoothedPath.GetPointByDist(&TrajPt);
            //printf("TRAJEKTORIJA: %lf, %lf , %lf, %lf, %lf, %lf\n", TrajPt.Time, TrajPt.x, TrajPt.y, TrajPt.Angle, TrajPt.Velocity, TrajPt.GetAngVelocity(), TrajPt.Curv);
            //printf("TRAJEKTORIJA: %f \n", TrajPt.Angle);
            p.x = TrajPt.x;
            p.y = TrajPt.y;
            path_data.points.push_back(p);
            vel.linear.x = TrajPt.Velocity;
            vel.angular.z = TrajPt.GetAngVelocity();

            traj_data.points.push_back(p);
            traj_data.velocities.push_back(vel);
            traj_data.t.push_back(TrajPt.Time);
            traj_data.th.push_back(TrajPt.Angle);
            traj_data.curv.push_back(TrajPt.Curv);

        }
        pub_path_result.publish(path_data);
        pub_traj_result.publish(traj_data);
        // int i = 0;
        // while(i<3){
        // pub_path_result.publish(path_data);
        // pub_traj_result.publish(traj_data);
        // i++;
        // }
        completed = SmoothedPath.LogTrajectoryByDist("smoothedPath.txt", Delta);      //spremanje putanje u txt za analizu u Matlabu
        
        // ROS_INFO("objavio %d put", pub);
        // pub ++;
        printf("zavrseno %d:", completed);
        //ROS_INFO("\n Saving smoothedPath.txt completed!!! Saljemo %d", path_data.points.size());
        if (completed == 1){
            C2DClothoidApp::DestroyLookupTable();
        }
   // }  
    //void main()
    //{
        // CTrajTrackingRegulator_Nonlinear Regulator;
        // CTrajTrackingRegulator_Nonlinear::SParms Parms;
        
        // //parametri ako se ne postave ostaju defaultni koji su definirani u TrajTrackingController.h
        // Parms.pTraj = nullptr;
        // Parms.SamplingPeriod = (1/80);
        // Parms.b = 60;
        // Parms.zeta = 0.7;
        // Parms.vMax = 2;
        // Parms.wMax = 2/(0.067/2);

        // Parms.LinAccelMax = 1;
        // Parms.AngAccelMax = 30;
        // Parms.nSamplesDelay = 0;



        // if(!Regulator.SetParms(&Parms))
        //     return;

        // for(int i=0; i<2400; i++)
        // {
        //     CMobileRobot::SRobotState MeasuredState;
        //     CMobileRobot::SRobotState RefState;

        //     MeasuredState.x = TrajPt.x;
        //     MeasuredState.y = TrajPt.y;
        //     MeasuredState.Angle = TrajPt.Angle;

        //     Regulator.GetRefValues(&MeasuredState, &RefState);

        //     // Posalji reference robotu
        //     float vRef = RefState.Velocity;
        //     float wRef = RefState.AngVelocity;

            
        //}
    //} 
    }

};
}
PLUGINLIB_EXPORT_CLASS(nodelet_path_smoothing::path_smoothing, nodelet::Nodelet);
