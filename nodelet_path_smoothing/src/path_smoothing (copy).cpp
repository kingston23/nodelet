#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include <std_msgs/String.h>
#include <stdio.h>
#include "nodelet_path_smoothing/OutputData.h"
#include "nodelet_path_smoothing/OutputPath.h"

#include "nodelet_talker/InputData.h"

#include "math.h"
#include <vector>
#include <list>
#include <sstream>

#include "Traj/PathSmoothing.h"


namespace nodelet_path_smoothing
{
class path_smoothing: public nodelet::Nodelet
{
public:
    ros::Publisher pub_result, pub_path_result;
    ros::Subscriber sub_chat;   
    typedef double T;
    double Delta = 0.05;
    int completed = 0;
    virtual void onInit() {  
        ros::NodeHandle& private_nh = getPrivateNodeHandle();
        pub_result = private_nh.advertise<OutputData>("result",10);
        pub_path_result = private_nh.advertise<OutputPath>("SmoothedPathPub",10);
        sub_chat = private_nh.subscribe<nodelet_talker::InputData>("msg_in", 1, &path_smoothing::callback, this);
        //ROS_INFO("Napravio pub i sub");
    }    


    void callback(const nodelet_talker::InputData::ConstPtr& input)
    {
        //ROS_INFO("Usao u callback");

        C2DTrajectoryBase::S2DTrajPoint* goalState = (C2DTrajectoryBase::S2DTrajPoint*)input->GoalStatePtr;
        C2DTrajectoryBase::S2DTrajPoint* currentState = (C2DTrajectoryBase::S2DTrajPoint*)input->CurrentStatePtr;
        std::vector<Geom::C2DPoint >* PWLinearPath= (std::vector<Geom::C2DPoint >*)input->PWLinearPathPtr;

        if(!C2DClothoidApp::CreateLookupTable())
            return;

        CClothoidPathSmoothing PathSmoothing;
        PathSmoothing.SmoothPath((*currentState), (*goalState), (*PWLinearPath));
        CTrajCompositePathProfile& SmoothedPath = PathSmoothing.SmoothedPath();

        ROS_INFO("goal: [%f %f]", goalState->x, goalState->y);

        OutputData data;
        OutputPath path_data;
        geometry_msgs::Point p; 

        path_data.points.clear();

        data.SmoothedPathPtr=(long)(&SmoothedPath);
        pub_result.publish(data);   //objavljivanje poruke na temu

        C2DTrajectoryBase::S2DTrajPoint TrajPt;
        TrajPt.SetAllZero();
        T Dist1 = SmoothedPath.GetBeginDist();
        T Dist2 = SmoothedPath.GetEndDist();
        for(T Dist = Dist1; Dist <= Dist2; Dist += Delta)
        {
            TrajPt.Dist = Dist;
            SmoothedPath.GetPointByDist(&TrajPt);
            //printf("TRAJEKTORIJA: %f, %f \n", TrajPt.x, TrajPt.y);
            p.x = TrajPt.x;
            p.y = TrajPt.y;
            path_data.points.push_back(p);
        }
        pub_path_result.publish(path_data);

        //completed = SmoothedPath.LogTrajectoryByDist("smoothedPath.txt", Delta);      //spremanje putanje u txt za analizu u Matlabu
        //SmoothedPath.LogTrajectoryByTime("smoothedPathTime.txt", 0.01);  
        //printf("zavrseno %d:", completed);
        ROS_INFO("\n Saving smoothedPath.txt completed!!! Saljemo %d tocaka", path_data.points.size());
        //if (completed == 1){
            C2DClothoidApp::DestroyLookupTable();
        //}
    }   
};
}
PLUGINLIB_EXPORT_CLASS(nodelet_path_smoothing::path_smoothing, nodelet::Nodelet);
